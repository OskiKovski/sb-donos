//
// Created by konrad on 15.01.16.
//

#include <inilib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include "donos_config.h"

#define NULLERR(x,msg) {if(x==NULL){fprintf(stderr,msg"\n"); if(cfg!=NULL)free_config(cfg); return 1;}}
#define ERRRET1(msg) {fprintf(stderr,msg"\n");exit(1);}

int load_donos_config(struct donos_config *config, const char *filename) {
    struct ini_section *cfg = load_config(filename);
    NULLERR(cfg,"Can't load config file.");

    struct ini_section *global_section = get_section_by_name(cfg, "global");
    NULLERR(global_section, "Config file doesn't contain global section.");

    // check address family
    char *af = get_value(global_section, "AF");
    NULLERR(af, "Address family option not present in config file.");
    if (strcmp(af, "4") == 0) config->addr_family = AF_INET;
    else if (strcmp(af, "6") == 0) config->addr_family = AF_INET6;
    else ERRRET1("Bad address family (AF option in config file).");

    // get trusted sb address
    char *trustedsb = get_value(global_section, "SBADDR");
    NULLERR(trustedsb, "Trusted SB address not present in config file.");
    if (config->addr_family == AF_INET) {
        struct sockaddr_in* ptr = (struct sockaddr_in *) &(config->trusted_sb);
        if (inet_pton(config->addr_family, trustedsb, &(ptr->sin_addr)) != 1) ERRRET1("Invalid trusted SB address.");
    }
    else {
        struct sockaddr_in6* ptr = (struct sockaddr_in6 *) &(config->trusted_sb);
        if (inet_pton(config->addr_family, trustedsb, &(ptr->sin6_addr)) != 1) ERRRET1("Invalid trusted SB address.");
    }
    free_config(cfg);
    return 0;
}
