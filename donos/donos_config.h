//
// Created by konrad on 15.01.16.
//

#ifndef DONOS_DONOS_CONFIG_H
#define DONOS_DONOS_CONFIG_H

#include <bits/socket.h>

struct donos_config {
    struct sockaddr_storage trusted_sb;
    int addr_family;
};

int load_donos_config(struct donos_config*, const char*);

#endif //DONOS_DONOS_CONFIG_H
