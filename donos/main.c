#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include "serialized_load.h"
#include "donos_config.h"

/*
 * /tmp/test.ini should look like this:
 *
 * [global]
 * AF=6
 * SBADDR=::1
 *
 */

static int status;

int serversock;
const char* serverport = "9666";
struct donos_config donoscfg;

void cleanup(int sig) {
    printf("Doing cleanup.\n");
    if (serversock != 0) close(serversock);
    exit(0);
}

void* get_in_addr(struct sockaddr *sa){
    if (sa->sa_family == AF_INET) return &(((struct sockaddr_in*)sa)->sin_addr);
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int check_trusted(struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        struct in_addr* connected = &(((struct sockaddr_in*)sa)->sin_addr);
        struct in_addr* trusted = &(((struct sockaddr_in*)&donoscfg.trusted_sb)->sin_addr);

        return memcmp(connected, trusted, sizeof(struct in_addr));
    }
    else {
        struct in6_addr* connected = &(((struct sockaddr_in6*)sa)->sin6_addr);
        struct in6_addr* trusted = &(((struct sockaddr_in6*)&donoscfg.trusted_sb)->sin6_addr);
        return memcmp(connected, trusted, sizeof(struct in6_addr));
    }
}

int main() {
    signal(SIGINT, (__sighandler_t) cleanup);

    // config
    if (load_donos_config(&donoscfg, "/tmp/test.ini")) {
        fprintf(stderr, "Can't load config.\n");
        exit(1);
    }

    // testing ipv6 stack
    struct addrinfo hints, *servinfo, *p;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = donoscfg.addr_family;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;    // on all interfaces!

    getaddrinfo(NULL, serverport, &hints, &servinfo);
    for (p = servinfo; p!=NULL; p = p->ai_next) {   // should be only one addrinfo
        // 1. create socket
        serversock = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (serversock == -1) {
            perror("socket");
            continue;
        }
        // /1. socket successfully created

        // 2. try to reuse address (if hanging in the air)
        status = setsockopt(serversock, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int));
        if (status == -1) {
            perror("setsockopt");
            exit(1);
        }
        // /2. reuse flag set

        // 3. bind to address
        status = bind(serversock, p->ai_addr, p->ai_addrlen);
        if (status == -1) {
            perror("bind");
            close(serversock);
            continue;
        }
        // /3. socket binded to address
        break;
    }
    freeaddrinfo(servinfo); // we're done with this

    if (p == NULL) {
        fprintf(stderr, "can't create socket.\n");
        exit(1);
    }

    // 4. listen on socket
    status = listen(serversock, 10);    // 10 queued connections
    if (status == -1) {
        perror("listen");
        exit(1);
    }

    // 5. accept loop
    char isrunning = 1;
    struct sockaddr_storage cliaddr;
    int sin_size = sizeof cliaddr;
    char s[INET6_ADDRSTRLEN];
    int clisock = 0;

    while (isrunning) {
        printf("Waiting for connection...\n");
        clisock = accept(serversock, (struct sockaddr *) &cliaddr, (socklen_t *) &sin_size);
        if (clisock == -1) {
            perror("accept");
            continue;
        }

        inet_ntop(cliaddr.ss_family, get_in_addr((struct sockaddr *) &cliaddr), s, sizeof(s));
        printf("Connection from: %s\n", s);

        if (check_trusted((struct sockaddr *) &cliaddr) != 0) {
            printf("Not trusted! Closing...\n");
            close(clisock);
            continue;
        }
        printf("Server trusted!\n");

        // /5. connection accepted

        // 6*. send some test data and close connection
        double loadavg[3];
        if (getloadavg(loadavg, 3) != 3) {
            perror("loadavg");
            close(clisock);
            continue;
        }

        struct serialized_load sload;
        serializeload(&sload, loadavg);
        if (send(clisock, &sload, sizeof(sload), 0) == -1) perror("send");
        close(clisock);
        // /6. done.
    }

    // never reached.
    return 0;
}