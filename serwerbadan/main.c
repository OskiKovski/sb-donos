#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <netinet/in.h>
#include "serverlist.h"
#include "serialized_load.h"
#include "serv_stats.h"
#include "sb_config.h"

in_port_t port = 9666;

unsigned noservers = 0;
unsigned remainingservers;

struct sb_config* cfg;

char* tab[3];

void cleanup(int sig) {
    if (cfg!=NULL) free_sb_cfg(cfg);
}

int main() {
    signal(SIGINT, cleanup);
    printf("Serwer badan.\n");
    // load config
    port = htons(port);
    cfg = load_sb_config("/tmp/config.cfg", "/tmp/servers.cfg");
    if (cfg==NULL) {
        fprintf(stderr, "Can't load config files.\n");
        exit(1);
    }

    noservers = cfg->noservers;
    struct serverlist* list;
    list = build_list(cfg, noservers, port);

    // config loaded.

    int* sockfds = malloc(noservers * sizeof(int));
    struct serv_stats* stats = malloc(noservers * sizeof(struct serv_stats));
    update_stats_ptrs(list, stats);

    int isrunning = 1;
    while(isrunning) {
        printf("sleep...\n");
        // poll each server for its load TODO
        struct serverlist* p;
        int i;
        for(p = list, i=0; p!=NULL; p=p->next, i++) {
            // connect and save fd in sockfds[i]
            sockfds[i] = socket((p->addr).ss_family, SOCK_STREAM, 0);

            // default - unavailable, we'll change it later
            stats[i].available = 0;

            if (sockfds[i] == -1) {
                perror("socket");
                continue;
            }
            int len = INET_ADDRSTRLEN;
            if (p->addr.ss_family == AF_INET6) len = INET6_ADDRSTRLEN;
            if (connect(sockfds[i], (const struct sockaddr *) &(p->addr), (socklen_t) len) == -1) {
                perror("connect");
                close(sockfds[i]);
                sockfds[i] = -1;
                continue;
            }
        }
        // here we have opened connection to reached servers
        remainingservers = 0;
        fd_set sockets, tempset;
        FD_ZERO(&sockets);
        int maxfd = 0;  // largest fd number
        for (i=0; i<noservers; i++) {
            if (sockfds[i] == -1) continue;
            FD_SET(sockfds[i], &sockets);
            remainingservers++;
            if (sockfds[i] > maxfd) maxfd = sockfds[i];
        }
        tempset = sockets;

        struct timeval tv;
        tv.tv_usec = 0;
        tv.tv_sec = cfg->interval;
        // wait for response from all (change fd in sockfds to -1 when done) or timeout
        while (remainingservers > 0) {
            tempset = sockets;
            int nosock = select(maxfd+1, &tempset, NULL, NULL, &tv);    // modifies tv!
            if (nosock == 0) break; // timeout
            if (nosock == -1) {
                perror("select");
                break;
            }
            // here we have number of sockets ready to read
            // iterate over fds and check
            for (i=0; i<noservers && nosock; i++) {
                if (sockfds[i] == -1) continue;
                if (FD_ISSET(sockfds[i], &tempset)) {
                    // read from socket and store data TODO
                    struct serialized_load sl;
                    ssize_t r = read(sockfds[i], &sl, sizeof(struct serialized_load));
                    printf("Bytes read: %d\n", (int) r);
                    if (r == sizeof(struct serialized_load)) {
                        deserializeload(stats[i].load, &sl);
                        stats[i].available = 1;
                    }
                    else if (r == 0) {
                        printf("I'm untrusted for this donos!\n");
                    }
                    else {
                        perror("read");
                        // what to do? nothing.
                    }

                    close(sockfds[i]);  // remember!
                    FD_CLR(sockfds[i], &sockets);
                    sockfds[i] = -1;
                    remainingservers--;
                    nosock--;
                }
            }
        }
        
        // compute weight for DNS request TODO
        // use it->stats->load[cfg->loadnumber]
        struct serverlist *it = list;
        while (it) {
            printf("Server %s\t", it->domainname);
            if (it->stats->available == 0) printf("unavailable!\n");
            else printf("load: %lf / %lf / %lf\tchosen: %lf\n", it->stats->load[0], it->stats->load[1], it->stats->load[2], it->stats->load[cfg->loadnumber]);
            it = it->next;
        }

        // send DNS request to server TODO

        select(0, NULL, NULL, NULL, &tv);   // sleeping. see: man select
    }
}