//
// Created by konrad on 15.01.16.
//

#include <stddef.h>
#include <inilib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sb_config.h"

#define NULLERR(x,msg) {if(x==NULL){fprintf(stderr,msg"\n"); return NULL;}}
//warning, I don't care about memory when error occurs

struct sb_config *load_sb_config(const char *configfilename, const char *serversfilename) {
    struct sb_config* sbcfg = malloc(sizeof(struct sb_config));
    sbcfg->serverslist = NULL;
    sbcfg->noservers = 0;

    struct ini_section* cfg = load_config(configfilename);
    NULLERR(cfg, "Can't load sb config file.");
    struct  ini_section* srvcfg = load_config(serversfilename);
    NULLERR(srvcfg, "Can't load servers from file.");

    // config
    struct ini_section* global = get_section_by_name(cfg, "global");
    NULLERR(global, "Section global not present in file.");

    char *interval_str = get_value(global, "interval");
    NULLERR(interval_str, "Interval not present in file.");

    char *loadnumber_str = get_value(global, "loadnumber");
    NULLERR(loadnumber_str, "Load number not present in file.");

    sbcfg->interval=atoi(interval_str);
    sbcfg->loadnumber=atoi(loadnumber_str);

    // servers
    struct ini_section* p = srvcfg;
    struct server* last = NULL;
    for (p=srvcfg; p!=NULL; p=p->next) {
        struct server* new = malloc(sizeof(struct server));
        new->next = NULL;
        if (sbcfg->serverslist == NULL) sbcfg->serverslist = new;
        sbcfg->noservers++;

        char* ip = get_value(p, "ip");
        NULLERR(ip, "Can't find ip for server.");
        new->ip = strdup(ip);

        char* domainname = get_value(p, "domainname");
        NULLERR(domainname, "Can't find domain name for server.");
        new->domainname = strdup(domainname);

        if (last != NULL) last->next = new;
        last = new;
    }

    free_config(cfg);
    free_config(srvcfg);
    return sbcfg;
}

void free_sb_cfg(struct sb_config *config) {
    // TODO!!!
}
