//
// Created by konrad on 15.01.16.
//

#ifndef SERWERBADAN_SB_CONFIG_H
#define SERWERBADAN_SB_CONFIG_H

/*
 * Two config files:
 * config.cfg - contains interval, loadnumber
 * servers.cfg - each section one server
 */

struct server {
    char* domainname;
    char* ip;
    struct server* next;
};

struct sb_config {
    int interval;   // in seconds
    int loadnumber; // 0, 1 or 2
    struct server* serverslist;
    int noservers;
};

struct sb_config* load_sb_config(const char*, const char*);
void free_sb_cfg(struct sb_config*);

#endif //SERWERBADAN_SB_CONFIG_H
