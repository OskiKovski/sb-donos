//
// Created by konrad on 13.01.16.
//

#ifndef SERWERBADAN_SERV_STATS_H
#define SERWERBADAN_SERV_STATS_H

struct serv_stats {
    double load[3];
    int available;
};

#endif //SERWERBADAN_SERV_STATS_H
