//
// Created by konrad on 12.01.16.
//

#include <stddef.h>
#include <stdlib.h>
#include "serverlist.h"
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>

struct serverlist *build_list(struct sb_config* cfg, unsigned int n, in_port_t port) {
    struct serverlist* list = NULL;
    struct serverlist* last = NULL;

    struct server* srv = cfg->serverslist;

    while(srv != NULL) {
        struct serverlist* new = malloc(sizeof(struct serverlist));
        memset(new, 0, sizeof(struct serverlist));  // fill with zeroes

        // if contains colon (':'), then ipv6
        sa_family_t family = AF_INET;
        if (strchr(srv->ip, ':') != NULL) family = AF_INET6;

        // just for checking
        char addr[INET6_ADDRSTRLEN];

        if (family == AF_INET) {
            struct sockaddr_in* ptr = (struct sockaddr_in *) &new->addr;
            inet_pton(AF_INET, srv->ip, &(ptr->sin_addr));
            ptr->sin_port = port;
            ptr->sin_family = AF_INET;
            printf("ipv4\n");

            // just for checking
            inet_ntop(new->addr.ss_family, &(ptr->sin_addr), addr, INET6_ADDRSTRLEN);
        }
        else {
            struct sockaddr_in6* ptr = (struct sockaddr_in6 *) &new->addr;
            inet_pton(AF_INET6, srv->ip, &(ptr->sin6_addr));
            ptr->sin6_port = port;
            ptr->sin6_family = AF_INET6;
            printf("ipv6\n");

            // just for checking
            inet_ntop(new->addr.ss_family, &(ptr->sin6_addr), addr, INET6_ADDRSTRLEN);
        }

        printf("Adding server %s\n", addr);

        new->domainname = strdup(srv->domainname);

        if (list == NULL) list = new;
        else last->next = new;

        last = new;
        srv = srv->next;
    }

    return list;
}

void free_list(struct serverlist *list) {
    struct serverlist* next;
    while (list != NULL) {
        next = list->next;
        free(list->domainname);
        free(list);
        list = next;
    }
}

void update_stats_ptrs(struct serverlist *structserverlist, struct serv_stats *stats) {
    while(structserverlist != NULL) {
        structserverlist->stats = stats;
        stats++;
        structserverlist = structserverlist->next;
    }
}
