//
// Created by konrad on 12.01.16.
//

#ifndef SERWERBADAN_SERVERLIST_H
#define SERWERBADAN_SERVERLIST_H

#include <sys/socket.h>
#include <netinet/in.h>
#include "sb_config.h"
#include "serv_stats.h"

struct serverlist {
    struct sockaddr_storage addr;
    char* domainname;
    struct serv_stats* stats;
    struct serverlist *next;
};

struct serverlist *build_list(struct sb_config*, unsigned int, in_port_t);
void free_list(struct serverlist*);
void update_stats_ptrs(struct serverlist*, struct serv_stats*);

#endif //SERWERBADAN_SERVERLIST_H
