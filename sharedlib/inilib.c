//
// Created by konrad on 10.01.16.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "inilib.h"

static void free_section(struct ini_keyval* keyval) {
    struct ini_keyval* next;
    for(; keyval != NULL; keyval=next) {
        next = keyval->next;
        free(keyval->key);
        free(keyval->value);
        free(keyval);
    }
}

void free_config(struct ini_section *section) {
    struct ini_section* next;
    for (; section != NULL; section=next) {
        next = section->next;
        free_section(section->list);
        free(section->name);
        free(section);
    }
}

static struct ini_section* new_section(char* leftbracket, char* rightbracket) {
    struct ini_section* new;
    new = malloc(sizeof(struct ini_section));

    new->next = NULL;
    new->list = NULL;
    assert(rightbracket>leftbracket);
    new->name = strndup(leftbracket+1, rightbracket-leftbracket-1);

    return new;
}

static struct ini_keyval* new_keyval(char* start, char* middle, char* end) {
    struct ini_keyval* new;
    new = malloc(sizeof(struct ini_keyval));

    new->next = NULL;
    assert(start<middle);
    assert(middle<end);
    new->key = strndup(start, middle-start);
    new->value = strndup(middle+1, end-middle-1);

    return new;
}

struct ini_section *load_config(const char *path) {
    struct ini_section* config = NULL;
    struct ini_section* actual_section = NULL;
    struct ini_keyval* actual_keyval = NULL;
    char *line;
    char *leftbracket, *rightbracket, *equalchar;
    size_t linelen = 0;
    ssize_t len;

    FILE* file = NULL;

    file = fopen(path, "r");
    if (file == NULL) {
        perror("fopen");
        return NULL;
    }

    while(1) {
        line = NULL;
        linelen = 0;
        if ((len = getline(&line, &linelen, file)) == -1) {    // if EOF or error
            if (line!=NULL) free(line);
            break;
        }
        // line successfully read
        leftbracket = strchr(line, '[');
        rightbracket = strchr(line, ']');
        equalchar = strchr(line, '=');

        if (equalchar != NULL) {
            struct ini_keyval *new = new_keyval(line, equalchar, line+len-1);
            // new keyvalue
            if (actual_keyval == NULL) {    // first keyval in section!
                actual_section->list = new;
            }
            else {
                actual_keyval->next = new;
            }
            actual_keyval = new;
        }
        else if (leftbracket != NULL && rightbracket != NULL) {
            struct ini_section *new = new_section(leftbracket, rightbracket);
            // new caption
            if (actual_section == NULL) {   // first section appeared
                config = new;
            }
            else {  // not first section
                actual_section->next = new;
                actual_keyval = NULL;
            }
            actual_section = new;

        }
        free(line);
    }

    fclose(file);
    return config;

    // for each line in file:

    // if new section:
    // make new section with given name
    // if actual section was null, update config variable and make new actual section, update actual keyval=null
    // else make new section with given name and link from actual, update actual section, update actual keyval=null

    // if new keyval:
    // make new keyval with given values
    // if actual keyval was null, update actual section->list
    // else link from actual keyval
}

void print_config(struct ini_section *section) {
    struct ini_section* p;
    struct ini_keyval* kv;
    for (p=section; p!=NULL; p=p->next) {
        printf("[%s]\n", p->name);
        for (kv=p->list; kv!=NULL; kv=kv->next) printf("%s=%s\n", kv->key, kv->value);
    }
}

struct ini_section *get_section_by_name(struct ini_section *config, const char *name) {
    while(config!=NULL) {
        if (strcmp(config->name, name) == 0) return config;
        config = config->next;
    }
    return NULL;
}

char *get_value(struct ini_section *section, const char *key) {
    struct ini_keyval* p;
    for (p=section->list; p!=NULL; p=p->next)
        if (strcmp(p->key, key) == 0) return p->value;
    return NULL;
}
