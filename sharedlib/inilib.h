//
// Small INI files library. Implemented for reading configuration.
// Created by konrad on 10.01.16.
//

#ifndef INIREADER_INILIB_H
#define INIREADER_INILIB_H

/*
 * Configuration is a set of sections. Each section contains set of key-value pairs.
 * It's implemented as a directed list of sections. Each section contains directed list of K-V pairs.
 *
 * Configuration is stored in dynamic memory. When you don't need it anymore,
 * you must free this memory up with free_config().
 *
 * Usage:
 * 1. Load config from file (load_config(filename))
 * 2. Find section you want (get_section_by_name(config, name))
 * 3. Get values for given keys from this section (get_value(section, key))
 * 4. Free memory (free_config(config))
 */

struct ini_keyval {
    char* key;
    char* value;
    struct ini_keyval *next;
};

struct ini_section {
    char* name;
    struct ini_keyval *list;
    struct ini_section *next;
};

// Loads configuration from file to memory. Allocates memory with malloc.
struct ini_section* load_config(const char* filename);
void print_config(struct ini_section* cfg); // debug purposes
// Frees dynamic memory for whole configuration
void free_config(struct ini_section* cfg);

// Returns pointer to section you want. Or NULL, if doesn't exist.
struct ini_section* get_section_by_name(struct ini_section* cfg, const char* name);
// Returns value string for given key. Or NULL, if doesn't exist.
char* get_value(struct ini_section* section, const char* key);

#endif //INIREADER_INILIB_H
