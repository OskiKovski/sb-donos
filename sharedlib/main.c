#include <stdio.h>
#include <stdlib.h>
#include "inilib.h"

int main() {
    struct ini_section *cfg = load_config("/tmp/asd.ini");
    if (cfg == NULL) {
        fprintf(stderr, "inireader: can't open file");
        exit(1);
    }
    print_config(cfg);

    struct ini_section *sec = get_section_by_name(cfg, "ddd");
    char *klucz = "testowa";
    char *val = get_value(sec, klucz);
    if (val == NULL)
        printf("Nie znaleziono klucza \"%s\" w sekcji %s\n", klucz, sec->name);
    else
        printf("wartosc dla %s to %s", klucz, val);
    
    free_config(cfg);
    return 0;
}