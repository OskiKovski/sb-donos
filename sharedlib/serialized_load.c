//
// Created by konrad on 08.01.16.
//

#include <netinet/in.h>
#include "serialized_load.h"

/*
 * serializuje obciazenie procesora tak, aby mozna je bylo wyslac przez siec.
 * 1.00 oznacza 100% wykorzystania procesora, >1.00 oznacza przeładowanie, a 0.33 oznacza, że procesor nudził się przez 67% czasu.
 * Zobacz: https://en.wikipedia.org/wiki/Load_(computing)
 * najpierw, zeby pozbyc sie przecinka, mnozymy przez 100 (interesuje nas tylko 2 miejsca po przecinku)
 * rozpatrujemy tylko przeladowania stukrotne (i tak za duzo, ale co tam)
 * wiec maksymalna wartosc to 100.00 (pomnozone przez 100 ucinające przecinek to 10000)
 * miescimy sie wiec w 16-bitowym incie.
 */
void serializeload(struct serialized_load *dst, const double *src) {
    int i;
    double d;

    for (i=0; i<3; ++i) {
        d = src[i];
        if (d>100) d=100.0;
        if (d<0) d=0.;  // powinno wywalic blad, ale uznajemy, ze pozyskujemy prawidlowe dane o avgload
        d *= 100;
        dst->load[i] = (uint16_t) d;    // na pewno się zmiesci, bo 0<d<10000
        dst->load[i] = htons(dst->load[i]);
    }
}

void deserializeload(double *dst, const struct serialized_load *src) {
    int i;
    uint16_t tmp;

    for (i=0; i<3; i++) {
        tmp = ntohs(src->load[i]);
        if (tmp > 10000) tmp = 10000;
        dst[i] = tmp;
        dst[i] *= .01;
    }
}
