//
// Created by konrad on 08.01.16.
//

#ifndef DONOS_SERIALIZED_LOAD_H
#define DONOS_SERIALIZED_LOAD_H

#include <stdint.h>

// struktura gotowa do wysłania przez sieć
struct serialized_load {
    uint16_t load[3];
};

void serializeload(struct serialized_load*, const double[]);
void deserializeload(double[], const struct serialized_load*);

#endif //DONOS_SERIALIZED_LOAD_H
