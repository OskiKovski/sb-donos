# Uruchomienie maszyn wirtualnych

### 1. Instalacja VirtualBoxa (wersja ze [strony VirtualBoxa](www.virtualbox.org))
### 2. Instalacja Vagranta
```bash
cd host
chmod +x install_vagrant.sh
./install_vagrant.sh
```
### 3. Uruchomienie wirtualki z serwerem FTP
```bash
cd boxes        # przejście do katalogu z plikami konfiguracyjnymi vagranta
vagrant up      # to może chwilę zająć, pobiera obraz z internetu i go instaluje
vagrant status  # co by sprawdzić, czy działa
vagrant ssh     # jeśli chcemy, możemy się zalogować do maszyny
```
### 4. Przetestowanie łączności z FTP
  1. Instalacja Filezilli
  2. Test połączenia:
    - IP: 192.168.33.11
    - login: test
    - hasło: test

### 5. Wyłączenie wirtualki
```bash
cd boxes
vagrant halt
```
### 6. Usunięcie wirtualki z komuptera
```bash
cd boxes
vagrant destroy
```
